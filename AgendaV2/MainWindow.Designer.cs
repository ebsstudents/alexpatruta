﻿namespace AgendaV2
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.indicateFilter = new System.Windows.Forms.Label();
            this.visualizeAgenda = new System.Windows.Forms.GroupBox();
            this.displayAgenda = new System.Windows.Forms.DataGridView();
            this.applyFilter = new System.Windows.Forms.Button();
            this.inputFilter = new System.Windows.Forms.TextBox();
            this.nameTag = new System.Windows.Forms.Label();
            this.telephoneTag = new System.Windows.Forms.Label();
            this.emailTag = new System.Windows.Forms.Label();
            this.nameInput = new System.Windows.Forms.TextBox();
            this.telephoneInput = new System.Windows.Forms.TextBox();
            this.emailInput = new System.Windows.Forms.TextBox();
            this.addEntry = new System.Windows.Forms.Button();
            this.removeEntry = new System.Windows.Forms.Button();
            this.updateEntry = new System.Windows.Forms.Button();
            this.nameErrorDisplay = new System.Windows.Forms.Label();
            this.telErrorDisplay = new System.Windows.Forms.Label();
            this.emailErrorDisplay = new System.Windows.Forms.Label();
            this.loadData = new System.Windows.Forms.Button();
            this.updateAgendaTools = new System.Windows.Forms.GroupBox();
            this.info = new System.Windows.Forms.Label();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizeAgenda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayAgenda)).BeginInit();
            this.updateAgendaTools.SuspendLayout();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // indicateFilter
            // 
            this.indicateFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.indicateFilter.AutoSize = true;
            this.indicateFilter.Location = new System.Drawing.Point(6, 18);
            this.indicateFilter.Name = "indicateFilter";
            this.indicateFilter.Size = new System.Drawing.Size(29, 17);
            this.indicateFilter.TabIndex = 0;
            this.indicateFilter.Text = "Filter";
            this.indicateFilter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.indicateFilter.UseCompatibleTextRendering = true;
            // 
            // visualizeAgenda
            // 
            this.visualizeAgenda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.visualizeAgenda.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.visualizeAgenda.Controls.Add(this.displayAgenda);
            this.visualizeAgenda.Controls.Add(this.applyFilter);
            this.visualizeAgenda.Controls.Add(this.inputFilter);
            this.visualizeAgenda.Controls.Add(this.indicateFilter);
            this.visualizeAgenda.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.visualizeAgenda.Location = new System.Drawing.Point(8, 32);
            this.visualizeAgenda.Name = "visualizeAgenda";
            this.visualizeAgenda.Size = new System.Drawing.Size(427, 417);
            this.visualizeAgenda.TabIndex = 1;
            this.visualizeAgenda.TabStop = false;
            this.visualizeAgenda.Text = "People List";
            // 
            // displayAgenda
            // 
            this.displayAgenda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.displayAgenda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.displayAgenda.BackgroundColor = System.Drawing.Color.White;
            this.displayAgenda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.displayAgenda.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.displayAgenda.Location = new System.Drawing.Point(6, 41);
            this.displayAgenda.Name = "displayAgenda";
            this.displayAgenda.RowHeadersVisible = false;
            this.displayAgenda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.displayAgenda.Size = new System.Drawing.Size(415, 370);
            this.displayAgenda.TabIndex = 3;
            // 
            // applyFilter
            // 
            this.applyFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.applyFilter.AutoSize = true;
            this.applyFilter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.applyFilter.FlatAppearance.BorderSize = 0;
            this.applyFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.applyFilter.Location = new System.Drawing.Point(349, 16);
            this.applyFilter.Name = "applyFilter";
            this.applyFilter.Size = new System.Drawing.Size(72, 22);
            this.applyFilter.TabIndex = 2;
            this.applyFilter.Text = "Apply Filter";
            this.applyFilter.UseVisualStyleBackColor = true;
            // 
            // inputFilter
            // 
            this.inputFilter.AccessibleDescription = "Input filter in the agenda";
            this.inputFilter.AccessibleName = "Filter Input text box";
            this.inputFilter.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.inputFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputFilter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inputFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputFilter.Location = new System.Drawing.Point(54, 16);
            this.inputFilter.Name = "inputFilter";
            this.inputFilter.Size = new System.Drawing.Size(289, 19);
            this.inputFilter.TabIndex = 1;
            this.inputFilter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nameTag
            // 
            this.nameTag.AutoSize = true;
            this.nameTag.Location = new System.Drawing.Point(3, 19);
            this.nameTag.Name = "nameTag";
            this.nameTag.Size = new System.Drawing.Size(35, 13);
            this.nameTag.TabIndex = 0;
            this.nameTag.Text = "Name";
            // 
            // telephoneTag
            // 
            this.telephoneTag.AutoSize = true;
            this.telephoneTag.Location = new System.Drawing.Point(3, 81);
            this.telephoneTag.Name = "telephoneTag";
            this.telephoneTag.Size = new System.Drawing.Size(58, 13);
            this.telephoneTag.TabIndex = 1;
            this.telephoneTag.Text = "Telephone";
            // 
            // emailTag
            // 
            this.emailTag.AutoSize = true;
            this.emailTag.Location = new System.Drawing.Point(3, 143);
            this.emailTag.Name = "emailTag";
            this.emailTag.Size = new System.Drawing.Size(32, 13);
            this.emailTag.TabIndex = 2;
            this.emailTag.Text = "Email";
            // 
            // nameInput
            // 
            this.nameInput.Location = new System.Drawing.Point(6, 35);
            this.nameInput.Name = "nameInput";
            this.nameInput.Size = new System.Drawing.Size(254, 20);
            this.nameInput.TabIndex = 3;
            this.nameInput.Leave += new System.EventHandler(this.nameInput_Leave);
            // 
            // telephoneInput
            // 
            this.telephoneInput.Location = new System.Drawing.Point(6, 97);
            this.telephoneInput.MaxLength = 10;
            this.telephoneInput.Name = "telephoneInput";
            this.telephoneInput.Size = new System.Drawing.Size(254, 20);
            this.telephoneInput.TabIndex = 4;
            this.telephoneInput.Leave += new System.EventHandler(this.telephoneInput_Leave);
            // 
            // emailInput
            // 
            this.emailInput.Location = new System.Drawing.Point(6, 159);
            this.emailInput.Name = "emailInput";
            this.emailInput.Size = new System.Drawing.Size(254, 20);
            this.emailInput.TabIndex = 5;
            this.emailInput.Leave += new System.EventHandler(this.emailInput_Leave);
            // 
            // addEntry
            // 
            this.addEntry.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addEntry.Enabled = false;
            this.addEntry.Location = new System.Drawing.Point(10, 387);
            this.addEntry.Name = "addEntry";
            this.addEntry.Size = new System.Drawing.Size(74, 24);
            this.addEntry.TabIndex = 6;
            this.addEntry.Text = "Add";
            this.addEntry.UseVisualStyleBackColor = true;
            this.addEntry.Click += new System.EventHandler(this.addEntry_Click);
            // 
            // removeEntry
            // 
            this.removeEntry.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.removeEntry.Enabled = false;
            this.removeEntry.Location = new System.Drawing.Point(90, 387);
            this.removeEntry.Name = "removeEntry";
            this.removeEntry.Size = new System.Drawing.Size(90, 24);
            this.removeEntry.TabIndex = 7;
            this.removeEntry.Text = "Remove";
            this.removeEntry.UseVisualStyleBackColor = true;
            // 
            // updateEntry
            // 
            this.updateEntry.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.updateEntry.Enabled = false;
            this.updateEntry.Location = new System.Drawing.Point(186, 387);
            this.updateEntry.Name = "updateEntry";
            this.updateEntry.Size = new System.Drawing.Size(74, 24);
            this.updateEntry.TabIndex = 8;
            this.updateEntry.Text = "Update";
            this.updateEntry.UseVisualStyleBackColor = true;
            // 
            // nameErrorDisplay
            // 
            this.nameErrorDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameErrorDisplay.ForeColor = System.Drawing.Color.Red;
            this.nameErrorDisplay.Location = new System.Drawing.Point(10, 58);
            this.nameErrorDisplay.Name = "nameErrorDisplay";
            this.nameErrorDisplay.Size = new System.Drawing.Size(250, 23);
            this.nameErrorDisplay.TabIndex = 9;
            // 
            // telErrorDisplay
            // 
            this.telErrorDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telErrorDisplay.ForeColor = System.Drawing.Color.Red;
            this.telErrorDisplay.Location = new System.Drawing.Point(10, 120);
            this.telErrorDisplay.Name = "telErrorDisplay";
            this.telErrorDisplay.Size = new System.Drawing.Size(250, 23);
            this.telErrorDisplay.TabIndex = 10;
            // 
            // emailErrorDisplay
            // 
            this.emailErrorDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailErrorDisplay.ForeColor = System.Drawing.Color.Red;
            this.emailErrorDisplay.Location = new System.Drawing.Point(10, 182);
            this.emailErrorDisplay.Name = "emailErrorDisplay";
            this.emailErrorDisplay.Size = new System.Drawing.Size(254, 23);
            this.emailErrorDisplay.TabIndex = 11;
            // 
            // loadData
            // 
            this.loadData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loadData.Location = new System.Drawing.Point(90, 358);
            this.loadData.Name = "loadData";
            this.loadData.Size = new System.Drawing.Size(90, 23);
            this.loadData.TabIndex = 12;
            this.loadData.Text = "Browse. . .";
            this.loadData.UseVisualStyleBackColor = true;
            this.loadData.Click += new System.EventHandler(this.loadData_Click);
            // 
            // updateAgendaTools
            // 
            this.updateAgendaTools.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.updateAgendaTools.Controls.Add(this.info);
            this.updateAgendaTools.Controls.Add(this.loadData);
            this.updateAgendaTools.Controls.Add(this.emailErrorDisplay);
            this.updateAgendaTools.Controls.Add(this.telErrorDisplay);
            this.updateAgendaTools.Controls.Add(this.nameErrorDisplay);
            this.updateAgendaTools.Controls.Add(this.updateEntry);
            this.updateAgendaTools.Controls.Add(this.removeEntry);
            this.updateAgendaTools.Controls.Add(this.addEntry);
            this.updateAgendaTools.Controls.Add(this.emailInput);
            this.updateAgendaTools.Controls.Add(this.telephoneInput);
            this.updateAgendaTools.Controls.Add(this.nameInput);
            this.updateAgendaTools.Controls.Add(this.emailTag);
            this.updateAgendaTools.Controls.Add(this.telephoneTag);
            this.updateAgendaTools.Controls.Add(this.nameTag);
            this.updateAgendaTools.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateAgendaTools.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.updateAgendaTools.Location = new System.Drawing.Point(443, 32);
            this.updateAgendaTools.Name = "updateAgendaTools";
            this.updateAgendaTools.Size = new System.Drawing.Size(266, 417);
            this.updateAgendaTools.TabIndex = 2;
            this.updateAgendaTools.TabStop = false;
            this.updateAgendaTools.Text = "Manage Agenda";
            // 
            // info
            // 
            this.info.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.info.Location = new System.Drawing.Point(6, 209);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(254, 146);
            this.info.TabIndex = 13;
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(5, 5);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(706, 24);
            this.menu.TabIndex = 3;
            this.menu.Text = "Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "&Open";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(149, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem.Image")));
            this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printToolStripMenuItem.Text = "&Print";
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripMenuItem.Image")));
            this.printPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printPreviewToolStripMenuItem.Text = "Print Pre&view";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator4,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(141, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(141, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customizeToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.customizeToolStripMenuItem.Text = "&Customize";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(119, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(716, 458);
            this.Controls.Add(this.updateAgendaTools);
            this.Controls.Add(this.visualizeAgenda);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "MainWindow";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "Agenda";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.visualizeAgenda.ResumeLayout(false);
            this.visualizeAgenda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayAgenda)).EndInit();
            this.updateAgendaTools.ResumeLayout(false);
            this.updateAgendaTools.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label indicateFilter;
        private System.Windows.Forms.GroupBox visualizeAgenda;
        private System.Windows.Forms.TextBox inputFilter;
        private System.Windows.Forms.Button applyFilter;
        private System.Windows.Forms.DataGridView displayAgenda;
        private System.Windows.Forms.Label nameTag;
        private System.Windows.Forms.Label telephoneTag;
        private System.Windows.Forms.Label emailTag;
        private System.Windows.Forms.TextBox nameInput;
        private System.Windows.Forms.TextBox telephoneInput;
        private System.Windows.Forms.TextBox emailInput;
        private System.Windows.Forms.Button addEntry;
        private System.Windows.Forms.Button removeEntry;
        private System.Windows.Forms.Button updateEntry;
        private System.Windows.Forms.Label nameErrorDisplay;
        private System.Windows.Forms.Label telErrorDisplay;
        private System.Windows.Forms.Label emailErrorDisplay;
        private System.Windows.Forms.Button loadData;
        private System.Windows.Forms.GroupBox updateAgendaTools;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

