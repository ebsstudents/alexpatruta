﻿namespace AgendaV2
{
    class Person
    {   /// <summary>
    /// number represents the entry identification number
    /// </summary>
        public static int identificationNumber = 0;
        public int ID { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }

        public Person(string name, string telephone, string email)
        {
            this.ID = identificationNumber++;
            this.Name = name;
            this.Telephone = telephone;
            this.Email = email;
        }
    }
}
