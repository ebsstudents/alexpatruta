﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System.Resources;
using System.Collections;

namespace AgendaV2
{
    class Agenda
    {
        /// <summary>
        /// Contains methods that will process the data
        /// </summary>

        private const string Name = "Name";
        private const string Telephone = "Telephone";
        private const string Email = "Email";
        private const string RootElement = "Agenda";
        public static string Company
        {
            get
            {
                return "Company";
            }
        }

        internal static void SetUpGridView(ref DataGridView displayAgenda)
        {
            displayAgenda.DefaultCellStyle.SelectionBackColor = SystemColors.ActiveCaption;
            displayAgenda.DefaultCellStyle.SelectionForeColor = Color.Black;
        }

        private const string Element = "Entry";
        private static string Path { get; set; }

        private static void ValidGraphics(Label displayMessage)
        {
            displayMessage.ForeColor = Color.SeaGreen;
            
            displayMessage.Text = "OK";
        }

        private static void InvalidGraphics(string message, Label displayMessage)
        {
            displayMessage.ForeColor = Color.IndianRed;
            displayMessage.Text = message;
            System.Media.SystemSounds.Beep.Play();
        }

        public static void ValidateEmail(string email, Label displayMessage)
        {
            try
            {
                var mail = new MailAddress(email);
                if (!string.IsNullOrEmpty(mail.Address))
                {
                    ValidGraphics(displayMessage);
                }
                else
                {
                    InvalidGraphics("Please input a valid e-mail address.", displayMessage);
                }
            }
            catch (Exception)
            {
                InvalidGraphics("Please input a valid e-mail address.", displayMessage);
            }
        }

        public static void ValidateName(string name, Label displayMessage)
        {
            if (!IsValid(name))
            {
                InvalidGraphics("Please input a valid name.", displayMessage);
            }
            else
            {
                ValidGraphics(displayMessage);
            }
        }

        public static void ValidateTelephone(string tel, Label displayMessage)
        {
            if (!isNumeric(tel))
            {
                InvalidGraphics("Please input a valid telephone number.", displayMessage);
            }
            else
            {
                ValidGraphics(displayMessage);
            }
        }

        internal static void NewAgenda(ref DataGridView agenda, ref List<Person> persons)
        {
            agenda.DataSource = null;
            agenda.Refresh();
            Path = null;
            persons.Clear();
        }

        public static void CheckData(Label name, Label tel, Label mail, Button addData)
        {
            if (name.Text == "OK" && tel.Text == "OK" && mail.Text == "OK")
            {
                addData.Enabled = true;
            }
            else
            {
                addData.Enabled = false;
            }
        }

        private static bool IsValid(string nameInput)
        {
            return (!string.IsNullOrEmpty(nameInput) && isAlphaNumeric(nameInput)) ? true : false;
        }

        private static bool isAlphaNumeric(string strToCheck)
        {
            Regex rg = new Regex(@"[\p{L}\p{N}_]");
            return rg.IsMatch(strToCheck);
        }

        private static bool isNumeric(string input)
        {
            var value = 0m;
            return (decimal.TryParse(input, out value)) ? true : false;
        }

        private static void CheckPath()
        {
            if (string.IsNullOrEmpty(Path))
            {
                DialogResult userChoice = MessageBox.Show("Please select the data source for your agenda.",
                                                        "Agenda database not found",
                                                        MessageBoxButtons.OKCancel,
                                                        MessageBoxIcon.Warning);
                if (userChoice == DialogResult.OK)
                {
                    BrowseForPath();
                }
            }
        }

        public static List<Person> Load()
        {
            ReadPathFromResource();
            CheckPath();
            List<Person> persons = new List<Person>();
            XDocument xmlAgenda = new XDocument();
            try
            {
                xmlAgenda = XDocument.Load(Path);
            }
            catch (Exception)
            {
                CheckPath();
            }
            
            try
            {
                var agenda = from personInformation in xmlAgenda.Descendants(Element)
                             select new
                             {
                                 Name = personInformation.Element(Name).Value.ToString(),
                                 Telephone = personInformation.Element(Telephone).Value.ToString(),
                                 Email = personInformation.Element(Email).Value.ToString()
                             };
                foreach (var person in agenda)
                {
                    persons.Add(new Person(person.Name, person.Telephone, person.Email));
                }
                return persons;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(),
                                "Agenda",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return persons;
            }
        }

        public static void BrowseForPath()
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "XML Files (.xml)|*.xml";
            openFile.ShowDialog();
            Path = openFile.FileName.ToString();
            SavePathToResource();
        }

        private static void SavePathToResource()
        {
            IResourceWriter writer = new ResourceWriter("Agenda.Path");
            writer.AddResource("Path", Path);
            writer.Close();
        }

        public static void Display(DataGridView displayData, List<Person> data)
        {
            displayData.DataSource = null;
            displayData.DataSource = data;
        }

        private static void ReadPathFromResource()
        {
            //IResourceReader reader = new ResourceReader("Agenda.Path");
            //IDictionaryEnumerator resource = reader.GetEnumerator();
            //while (resource.MoveNext())
            //{
            //    if (resource.Key.ToString() == "Path")
            //    {
            //        Path = resource.Value.ToString();
            //    }
            //}
            //reader.Close();
            //if (string.IsNullOrEmpty(Path))
            //{
            //    BrowseForPath();
            //}
        }

        public static void AddEntry(ref List<Person> persons,
                                    TextBox name,
                                    TextBox tel,
                                    TextBox email)
        {
            persons.Add(new Person(
                name.Text.ToString(),
                tel.Text.ToString(),
                email.Text.ToString()));
        }
    }
}
