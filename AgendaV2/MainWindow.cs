﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AgendaV2
{
    public partial class MainWindow : Form
    {
        List<Person> persons = new List<Person>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            Agenda.SetUpGridView(ref displayAgenda);
            persons = Agenda.Load();
            Agenda.Display(displayAgenda, persons);
        }

        private void nameInput_Leave(object sender, EventArgs e)
        {
            Agenda.ValidateName(nameInput.Text.ToString(), nameErrorDisplay);
            Agenda.CheckData(nameErrorDisplay, telErrorDisplay, emailErrorDisplay, addEntry);
        }

        private void telephoneInput_Leave(object sender, EventArgs e)
        {
            Agenda.ValidateTelephone(telephoneInput.Text.ToString(), telErrorDisplay);
            Agenda.CheckData(nameErrorDisplay, telErrorDisplay, emailErrorDisplay, addEntry);
        }

        private void emailInput_Leave(object sender, EventArgs e)
        {
            Agenda.ValidateEmail(emailInput.Text.ToString(), emailErrorDisplay);
            Agenda.CheckData(nameErrorDisplay, telErrorDisplay, emailErrorDisplay, addEntry);
        }

        private void loadData_Click(object sender, EventArgs e)
        {
            Agenda.BrowseForPath();
            Agenda.Display(displayAgenda, persons);
        }

        private void addEntry_Click(object sender, EventArgs e)
        {
            Agenda.AddEntry(ref persons, 
                     nameInput,
                     telephoneInput,
                     emailInput
                     );
            Agenda.Display(displayAgenda, persons);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Agenda.NewAgenda(ref displayAgenda, ref persons);
        }
    }
}
